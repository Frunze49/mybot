import random
import time

from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
import sqlite3 as sq
from aiogram.contrib.fsm_storage.memory import MemoryStorage

import os

API_TOKEN = '5768186705:AAGJmxM4IifGsHaeezw7KpRizZ4z8IZL4HU'

current_command = ""
current_command_step = -1

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())


class Finder(StatesGroup):
    username = State()


class Human(StatesGroup):
    description = State()
    photo = State()


class Meet(StatesGroup):
    Theme = State()
    Time = State()
    Link = State()
    Nicks = State()


class Stick(StatesGroup):
    stick_id = State()

meet_struct = Meet()
human_struct = Human()


async def on_startup(_):
    global base, cur
    base = sq.connect('Users.db')
    cur = base.cursor()
    base.execute(
        'CREATE TABLE IF NOT EXISTS all_users(username TEXT PRIMARY KEY, img text, name TEXT, userid TEXT, description TEXT)')
    base.commit()
    print("Online")


@dp.message_handler(commands=['start'], state='*')
async def send_welcome(message: types.Message):
    await message.answer("Привет! Чем я могу тебе помочь?\n"
                         "Ты можешь:\n"
                         "/find - Узнать информацию о человеке из моей базы по тэгу(без @)\n"
                         "/info - Попасть в мою базу, чтобы другие легко могли тебя найти\n"
                         "/meeting - Пригласить людей на встречу, указав тему, время, ссылку на митап и теги участников\n"
                         "/sticker - Послать всем людям из базы свой крутой стикер:)\n"
                         "/drop_all - с вероятностью 1/10000 дропнуть базу и оффнуть бота:)\n")


@dp.message_handler(commands=['meeting'], state='*')
async def make_meeting(message: types.Message, state: FSMContext):
    await message.answer("Отлично, теперь напиши тему встречи")
    await Meet.Theme.set()


@dp.message_handler(commands=['drop_all'], state='*')
async def send_welcome(message: types.Message):
    tmp = random.randint(1, 10000)
    if tmp == 1:
        await message.answer("Ах ты ж...")
        cur.execute('DROP TABLE all_users')
        exit(0)
    else:
        await message.answer("Попытайся еще:)")

@dp.message_handler(commands=['find'], state='*')
async def send_welcome(message: types.Message):
    await message.answer("Введи username человека, которого хочешь найти")
    await Finder.username.set()

@dp.message_handler(commands=['sticker'], state='*')
async def send_welcome(message: types.Message):
    await message.answer("Отправь лучший свой стик")
    await Stick.stick_id.set()


@dp.message_handler(content_types=['sticker'], state=Stick.stick_id)
async def helper(message: types.Message, state: FSMContext):
    for every in cur.execute('SELECT * FROM all_users').fetchall():
        await bot.send_sticker(every[3], message.sticker.file_id)

    await state.finish()


@dp.message_handler(commands=['info'], state='*')
async def put_info_to_dp(message: types.Message, state: FSMContext):
    await Human.description.set()
    await message.answer("Загрузи свое описание")


@dp.message_handler(state=Finder.username)
async def helper(message: types.Message, state: FSMContext):
    tmp = []
    for ret in cur.execute('SELECT * FROM all_users').fetchall():
        if ret[0] == message.text:
            tmp = ret

    if tmp:
        await bot.send_photo(message.from_user.id, tmp[1], "А вот и тот, кого ты искал:\n"
                             + str(tmp[2]) + "\n" + str(tmp[0]) + "\n" + str(tmp[4]))
    else:
        await message.answer("Такого нет в нашей базе:(")
    await state.finish()


@dp.message_handler(state=Human.description)
async def helper(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['description'] = message.text
    await Human.photo.set()
    await message.answer("Загрузи фото")


@dp.message_handler(content_types=['photo'], state=Human.photo)
async def helper(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['photo'] = message.photo[0].file_id
        data['name'] = message.from_user.full_name
        data['username'] = message.from_user.username
        data['userid'] = message.from_user.id
        tmp = data

    await bot.send_photo(message.from_user.id, tmp['photo'], "Ну, а вот и ты:\n"
                         + str(tmp['name']) + "\n" + str(tmp['username']) + "\n" + str(tmp['description']))

    data = (tmp['username'], tmp['photo'], tmp['name'], tmp['userid'], tmp['description'])
    cur.execute('INSERT OR IGNORE INTO all_users VALUES (?, ?, ?, ?, ?)', data)
    base.commit()
    await state.finish()


@dp.message_handler(state=Meet.Theme)
async def make_meeting(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Theme'] = message.text
    await message.answer("Ок-ок, а что там по времени?")
    await Meet.Time.set()


@dp.message_handler(state=Meet.Time)
async def make_meeting(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Time'] = message.text
    await message.answer("Окей, кидай ссылку на встречу!")
    await Meet.Link.set()


@dp.message_handler(state=Meet.Link)
async def make_meeting(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Link'] = message.text
    await message.answer("Каких людей позвать на встречу? (usernames без @, через запятые)")
    await Meet.Nicks.set()


@dp.message_handler(state=Meet.Nicks)
async def make_meeting(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Link'] = message.text
    tags_array = message.text.split(',')
    for name in tags_array:
        for every in cur.execute('SELECT * FROM all_users').fetchall():
            if every[0] == name:
                await bot.send_message(every[3],
                                       "Йоу, у тебя встреча на тему \"" + str(data['Theme'])
                                       + "\" в " + str(data['Time']) + " не забудь!\n" + str(data['Link']))
                break
    await message.answer("Сделано, босс")
    await state.finish()


@dp.message_handler(state='*')
async def brrr(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Бррр, так, давай по новой")

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
